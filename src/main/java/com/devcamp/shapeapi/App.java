package com.devcamp.shapeapi;

import com.devcamp.shapeapi.model.Circle;
import com.devcamp.shapeapi.model.Rectangle;
import com.devcamp.shapeapi.model.Shape;
import com.devcamp.shapeapi.model.Square;

public class App {
    public static void mainh(String[] args) throws Exception {
        System.out.println("Hello, World!");
        // yêu cầu 6 hực hiện khởi tạo 2 đối tượng hình học shape1 (không tham số) và shape2 (có tham số - filled = false và color = “green”)
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green",false);
        //Sử dụng phương thức toString để in 3 đối tượng trên ra console 
        System.out.println(shape1);
        System.out.println(shape2);
        // yc 7 thực hiện khởi tạo 3 đối tượng hình tròn
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3, "green", true);
        //Sử dụng phương thức toString để in 3 đối tượng trên ra console 
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);

        //yc 8 thực hiện khởi tạo 3 đối tượng hình chữ nhật
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle(2.0, 1.5, "green", true );

        System.out.println(rectangle1);
        System.out.println(rectangle2);
        System.out.println(rectangle3);
        // yc 9 thực hiện khởi tạo 3 đối tượng hình vuông\

        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square(2.0, "green", true );

        System.out.println(square1);
        System.out.println(square2);
        System.out.println(square3);

        // yc 10 In ra diện tích (getArea) và chu vi (getPerimeter) của 3 đối tượng hình tròn được khởi tạo ở test 7
        System.out.println("In ra diện tích (getArea) và chu vi (getPerimeter) của 3 đối tượng hình tròn ");
        System.out.println(circle1.getArea());
        System.out.println(circle2.getArea());
        System.out.println(circle3.getArea());

        System.out.println(circle1.getPerimeter());
        System.out.println(circle2.getPerimeter());
        System.out.println(circle3.getPerimeter());

        // yc 11 
        System.out.println("In ra diện tích (getArea) và chu vi (getPerimeter) của 3 đối tượng hình chữ nhật  ");
        System.out.println(rectangle1.getArea());
        System.out.println(rectangle2.getArea());
        System.out.println(rectangle3.getArea());

        System.out.println(rectangle1.getPerimeter());
        System.out.println(rectangle2.getPerimeter());
        System.out.println(rectangle3.getPerimeter());
        // yc 12 
        System.out.println("In ra diện tích (getArea) và chu vi (getPerimeter) của 3 đối tượng hình vuông ");
        System.out.println(square1.getArea());
        System.out.println(square2.getArea());
        System.out.println(square3.getArea());

        System.out.println(square1.getPerimeter());
        System.out.println(square2.getPerimeter());
        System.out.println(square3.getPerimeter());




    }
}
