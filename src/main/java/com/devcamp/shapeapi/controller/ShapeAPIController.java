package com.devcamp.shapeapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shapeapi.model.Circle;
import com.devcamp.shapeapi.model.Rectangle;
import com.devcamp.shapeapi.model.Square;

@CrossOrigin
@RestController
public class ShapeAPIController {
    // yc 4 Sau khi thực hiện khởi tạo xong, trả ra giá trị diện tích hình tròn.
    @GetMapping("/circle-area")
    public double getCircleArea(@RequestParam(name = "radius") double radius) {
        // khởi tạo đối tượng hình tròn
        Circle circle = new Circle(radius);
        return circle.getArea();
    }

    // yc 5 Sau khi thực hiện khởi tạo xong, trả ra giá trị chu vi hình tròn.
    @GetMapping("/circle-perimeter")
    public double getPerimeterCircle(@RequestParam(name = "radius") double radius) {
        // khởi tạo đối tượng hình tròn
        Circle circle = new Circle(radius);
        return circle.getPerimeter();
    }

    // yc 6 Sau khi thực hiện khởi tạo xong, trả ra giá trị diện tích chữ nhật 
    @GetMapping("/rectangle-area")
    public double getRectangleArea(@RequestParam(name = "width") double width,
            @RequestParam(name = "length") double length) {
        // khởi tạo đối tượng hình tròn
        Rectangle rectangle2 = new Rectangle(width, length);
        return rectangle2.getArea();
    }

     // yc 7 Sau khi thực hiện khởi tạo xong, trả ra giá trị diện tích chữ nhật 
     @GetMapping("/rectangle-perimeter")
     public double getRectanglePerimete(@RequestParam(name = "width") double width,
             @RequestParam(name = "length") double length) {
         // khởi tạo đối tượng hình tròn
         Rectangle rectangle2 = new Rectangle(width, length);
         return rectangle2.getPerimeter();
     }

      // yc 7 Sau khi thực hiện khởi tạo xong, trả ra giá trị diện tích hình vuông
      @GetMapping("/square-area")
      public double getSquareArea(@RequestParam() double side ) {
          // khởi tạo đối tượng hình tròn
          Square square2 = new Square(side);
          return square2.getArea();
      }

      // yc 8 Sau khi thực hiện khởi tạo xong, trả ra giá trị diện tích hình vuông
      @GetMapping("/square-perimeter")
      public double getSquarePerimeter(@RequestParam() double side ) {
          // khởi tạo đối tượng hình tròn
          Square square2 = new Square(side);
          return square2.getArea();
      }

}
