package com.devcamp.shapeapi.model;

public class Square extends Rectangle {

    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(double side, String color, boolean filled) {
        super( side, side, color, filled);
    }

    public void setSide(double side) {
        setLength(side);
        setWidth(side);
    }

    public double getSide(){
        return getLength();
    }

    public void setWidth(double side) {
        setLength(side);
        setWidth(side);
    }

    public void setLength(double side) {
        setLength(side);
        setWidth(side);
    }

    
    @Override
    public String toString() {
        return "Square[ Rectangle [ Shape [color=" + getColor() + ", filled=" + isFilled() + "], width=" + getWidth() + ", length=" + getLength() + "]]";
    }

    

    
    
    
}
